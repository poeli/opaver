# Omics Pathway Viewer - Annotation version

This is an modified version of OPaver that can visualize genome annotation in KEGG map. The software will take annotation in GFF3 format and use the EC# information to gather associated pathway maps from KEGG. Despite GFF3 is standard, the format of attribute fields is not. Currently, the script can fully support GFF3 files generated by Prokka. For other gff3 files, the EC number have to be provided in `ec_number` tag (case insensitive), for example `eC_number=5.4.2.11;`.

Unlike original OPaver, all pathway images and xml are natively downloaded from KEGG to the output directory. If you already have a previous run, copying previous pathway files (*.png & *.xml) to current output directory could save you a lot of time.

<img src='images/OPaver_annotation.gif'>

### Quick Example

The dataset for above animations is provided and pre-processed. Simply follow the command below after you download OPaver repo:

```bash
tar -xzf test/B.anthracis.8cpu.output.tar.gz -C test/
cd opaver_web
python -m http.server 8000 &
open "http://localhost:8000/pathway_anno.html?data=B.anthracis.8cpu.output"
```

### Dependency

No dependency.

### Usage

```bash
OPaver (Omics Pathway Viewer) is a web-based viewer to provide 
an integrated and interactive visualization of omics data in KEGG maps.

Version: v0.3

opaver_anno.pl [OPTIONS] -g <GFF3>
	--gff3|g    genome annotation in gff3 file (with EC#)

[OPTIONS]
	--outdir|o
	--prefix|p
	--ecinfojson|e
	--pwyinfojson|y
```

### Example for Bacillus anthracis annotation

Change directory to the example dir: 

```
cd test/
```

Test GFF3 file is a Bacillus anthracis genome annotation generated at bioedge.lanl.gov (B.anthracis.8cpu).

```
../scripts/opaver_anno.pl -g B.anthracis.8cpu.gff --outdir B.anthracis.8cpu.output

# link output to the viewer's data directory
cd ../opaver_web/data/
ln -s ../test/B.anthracis.8cpu.output .
```

Open viewer with default browser in MacOS:

```
cd ../
python -m http.server 8000 &
open "http://localhost:8000/pathway_anno.html?data=B.anthracis.8cpu.output"
```
