SELECT 'DROP TABLE "' || name || '";' 
FROM sqlite_master
WHERE type = 'table' 
  AND (name LIKE '%_genes_%' OR name LIKE '%_dblinks_%');
