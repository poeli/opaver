#!/usr/bin/env python3
import argparse as ap
import pandas as pd
import xml.etree.ElementTree as ET 
import sys
import os
import io
import json
import logging
import warnings

__version__ = "0.5.2"


def parse_params(__version__):
    p = ap.ArgumentParser(
        prog=sys.argv[0], description=f"Omics Pathway Viewer (OPaver) v{__version__}")

    p.add_argument(
        '-e', '--ec', metavar='FILE', nargs='*', type=str, default=[],
        help="""EC KGML files.""")

    p.add_argument(
        '-k', '--ko', metavar='FILE', nargs='*', type=str, default=[],
        help="""KO KGML files""")

    args_parsed = p.parse_args()

    return args_parsed


class KeggmapEcBlock:
    """ The class of Expression info """
    
    # Collection of instances
    _member_list = {}
    _member_ec = []

    @classmethod
    def get_instances(cls):
        return cls._member_list.values()

    @classmethod
    def empty_instances(cls):
        cls._member_list = []

    @classmethod
    def add_ko(cls, mapid, x, y, ko):
        key = f"{mapid}-{x}-{y}"
        # add KO info to KeggmapEcBlock object
        if key in cls._member_list:
            cls._member_list[key].ko = ko.lstrip("ko:").split(" ko:")

    @classmethod
    def add_ec(cls, mapid, x, y, ec):
        key = f"{mapid}-{x}-{y}"
        ec_list = ec.lstrip("ec:").split(" ec:")
        for ec in ec_list:
            if not key in self.__class__._member_list:
                self.__class__._member_list[key] = {}
                if not ec in self.__class__._member_ec:
                    self.__class__._member_list[key] = self
                    self.__class__._member_ec.append(ec)


    def __init__(self, mapid, x, y, ec):
        self.mapid = mapid
        self.x = x
        self.y = y
        self.ec = ec
        self.ko = []

    def get_ec_ko(self):
        return {self.ec: self.ko}

if __name__ == '__main__':
    warnings.simplefilter(action='ignore', category=FutureWarning)
    argvs = parse_params(__version__)
    log_level = logging.INFO

    # get logger
    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(log_level)
    formatter = logging.Formatter(
        '%(asctime)s [%(levelname)s] %(message)s', '%H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    for kgml in argvs.ec:
        mapid = kgml.split("/ec")[1].replace(".xml","")
        logger.info(f"{mapid}: {kgml}")
        try:
            tree = ET.parse(kgml)
        except:
            logger.warning(f"FAILED: {kgml}")
            continue

        root = tree.getroot()
        for entry in root.findall('entry'):
            # <entry id="104" name="ec:7.2.2.19" type="enzyme"
            #     link="http://www.kegg.jp/dbget-bin/www_bget?7.2.2.19">
            #     <graphics name="7.2.2.19" fgcolor="#000000" bgcolor="#BFBFFF"
            #          type="rectangle" x="1092" y="409" width="46" height="17"/>            
            if entry.get('type') != "enzyme":
                continue

            for graphics in entry:
                x = graphics.get('x')
                y = graphics.get('y')
                ec = graphics.get('name')
                logger.debug(f"{mapid}, {x}, {y}, {ec}")
                KeggmapEcBlock(mapid, x, y, ec)

    for kgml in argvs.ko:
        mapid = kgml.split("/ko")[1].replace(".xml","")
        logger.info(f"{mapid}: {kgml}")
        try:
            tree = ET.parse(kgml)
        except:
            logger.warning(f"FAILED: {kgml}")

        root = tree.getroot()
        for entry in root.findall('entry'):
            # <entry id="107" name="ko:K00937 ko:K22468" type="ortholog"
            #     link="http://www.kegg.jp/dbget-bin/www_bget?K00937+K22468">
            #     <graphics name="K00937..." fgcolor="#000000" bgcolor="#BFBFFF"
            #          type="rectangle" x="864" y="455" width="46" height="17"/>          
            if entry.get('type') != "ortholog":
                continue
            
            ko = entry.get('name')

            for graphics in entry:
                x = graphics.get('x')
                y = graphics.get('y')
                logger.debug(f"{mapid}, {x}, {y}, {ko}")
                KeggmapEcBlock.add_ko(mapid, x, y, ko)

    for ec in KeggmapEcBlock.get_instances():
        print(ec.get_ec_ko())