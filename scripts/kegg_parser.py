#!/usr/bin/env python3
import numpy as np
import pandas as pd
import logging
import sys
import os
import logging
import utility
import click

# get logging
logging.basicConfig(
    level=logging.INFO,
    format='%(asctime)s [%(levelname)s] %(module)s: %(message)s',
    datefmt='%Y-%m-%d %H:%M',
)

@click.command()
@click.option('-k', '--kegginfo', required=True, help='the path of kegg_info/')
@click.option('--lite', is_flag=True, help='create lite-version of sqlite database')
def build_db(kegginfo, lite):
    kegg_info_path = kegginfo
    kegg_db_path = f'{kegg_info_path}/kegg_info.db'

    # Parsing pathway ---
    pathway_record = utility.KeggInfoRecord(kegg_db_path)
    kegg_pathway_path = f'{kegg_info_path}/kegg_pathway_list.txt'
    pathway_record.add_pathway(kegg_pathway_path)
    logging.info(f"Storing db:kegg_pathway")
    pathway_record.insert_pwy()
    del pathway_record

    # Parsing CPD ---
    #
    # Example:
    #
    # ENTRY       C00002                      Compound
    # NAME        ATP;
    #             Adenosine 5'-triphosphate
    # FORMULA     C10H16N5O13P3
    # EXACT_MASS  506.9957
    # MOL_WEIGHT  507.181
    # REMARK      Same as: D08646
    # REACTION    R00002 R00076 R00085 R00086 R00087 R00088 R00089 R00104
    #             R00105 R00124 R00126 R00127 R00128 R00129 R00130 R00137
    # PATHWAY     map00190  Oxidative phosphorylation
    #             map00195  Photosynthesis
    # MODULE      M00049  Adenine ribonucleotide biosynthesis, IMP => ADP,ATP
    #             M00889  Puromycin biosynthesis, ATP => puromycin
    # ENZYME      1.1.98.6        1.2.1.30        1.2.1.95        1.2.1.101
    #             1.3.7.7         1.3.7.8         1.3.7.14        1.3.7.15
    #             1.13.12.7       1.17.4.2        1.18.6.1        1.18.6.2
    # BRITE       Compounds with biological roles [BR:br08001]
    #                 C00002  ATP
    # DBLINKS     CAS: 56-65-5
    #             PubChem: 3304
    #             NIKKAJI: J10.680A
    # ATOM        31
    #             1   N4y N    29.4250  -14.6015
    #             30  O1c O    15.1441  -15.4430
    # BOND        33
    #             1     1   2 1
    #             33   12  13 1
    # ///
    cpd_record = utility.KeggInfoRecord(kegg_db_path)
    dir_path = f"{kegg_info_path}/kegg_info_cpd"

    for filename in os.listdir(dir_path):
        if filename.endswith(".txt"):
            cpd_record.add_record(f"{dir_path}/{filename}")

    if not lite:
        logging.info(f"Storing db:dblinks")
        cpd_record.insert_cpd_dblinks()
    
    logging.info(f"Storing db:c_info")
    cpd_record.insert_cpd_info()

    del cpd_record

    # Parsing EC ---
    ec_record = utility.KeggInfoRecord(kegg_db_path)
    dir_path = f"{kegg_info_path}/kegg_info_ec"

    for filename in os.listdir(dir_path):
       if filename.endswith(".txt"):
           ec_record.add_record(f"{dir_path}/{filename}")

    if not lite:
        logging.info(f"Storing db:ec_genes")
        ec_record.insert_ec_genes()

    logging.info(f"Storing db:ec_info")
    ec_record.insert_ec_info()

    del ec_record

    # Parsing KO ---
    ko_record = utility.KeggInfoRecord(kegg_db_path)
    dir_path = f"{kegg_info_path}/kegg_info_ko"

    for filename in os.listdir(dir_path):
       if filename.endswith(".txt"):
           ko_record.add_record(f"{dir_path}/{filename}")

    if not lite:
        logging.info(f"Storing db:ko_genes")
        ko_record.insert_ko_genes()
    
    logging.info(f"Storing db:ec_info")
    ko_record.insert_ko_info()

    del ko_record

if __name__ == "__main__":
    build_db()
    logging.info(f"Done.")
