#!/usr/bin/env python3
import numpy as np
import pandas as pd
import sqlite3
import os
import logging

class DbAccess():
    """
    This is the class to create sqlite3 db for KEGG pathway and records
    """

    def __init__(self, db):
        self.db = db
        self.conn = None
        self.c = None

    def init_db(self, overwrite=False):
        if os.path.exists(self.db) and overwrite:
            os.remove(self.db)

        try:
            self.conn = sqlite3.connect(self.db)
            self.c = self.conn.cursor()

            self.c.execute("""
            CREATE TABLE IF NOT EXISTS kegg_pathway(
            ID         CHAR(5)     NOT NULL,
            NAME       CHAR(200)   NOT NULL,
            PRIMARY KEY (ID)
            );
            """)

            self.c.execute("""
            CREATE TABLE IF NOT EXISTS ec_info(
            ID        CHAR(20)    NOT NULL, /* ec id */
            NAME      CHAR(30)    NULL, /* ec name */
            CLASS     CHAR(30)    NULL, /* ec class */
            COMMENT   CHAR(200)   NULL,
            PATHWAY   CHAR(200)   NULL,
            PRIMARY KEY (ID)
            );
            """)

            self.c.execute("""
            CREATE TABLE IF NOT EXISTS ko_info(
            ID        CHAR(6)     NOT NULL, /* ko id */
            NAME      CHAR(100)   NULL, /* ko SYMBOL */
            DEF       CHAR(200)   NULL, /* ko NAME */
            PATHWAY   CHAR(200)   NULL,
            PRIMARY KEY (ID)
            );
            """)

            self.c.execute("""
            CREATE TABLE IF NOT EXISTS c_info(
            ID        CHAR(6)    NOT NULL, /* Compound id */
            NAME      CHAR(100)   NULL, /* Compound name */
            FORMULA   CHAR(100)   NULL,
            PATHWAY   CHAR(200)   NULL,
            PRIMARY KEY (ID)
            );
            """)

        except sqlite3.Error as e:
            logging.error("Database error: %s" % e)
        except Exception as e:
            logging.error("Exception in _query: %s" % e)

    def init_org_genes_table(self, tablename):
        self.c.execute(f"""
        CREATE TABLE IF NOT EXISTS '{tablename}'(
        ID        CHAR(20)   NOT NULL, /* EC# */
        ORG       CHAR(10)   NULL, /* ORG letters */
        GENE      CHAR(20)   NULL, /* GENE symbol */
        PRIMARY KEY (ID, ORG, GENE)
        );
        """)

        return True


    def init_dblink_table(self, tablename):
        self.c.execute(f"""
        CREATE TABLE IF NOT EXISTS '{tablename}'(
        ID       CHAR(20)   NOT NULL, /* C# */
        DB       CHAR(10)   NULL, /* Other DB */
        CID      CHAR(20)   NULL, /* corresponding ID */
        PRIMARY KEY (ID, DB, CID)
        );
        """)

        return True


    def close_db(self):
        self.conn.commit()
        self.conn.close()


class KeggInfoRecord(DbAccess):
    """
    The class for KEGG info file
    """

    def __init__(self, db):
        DbAccess.__init__(self, db)
        self.df = pd.DataFrame()
        self.init_db()

    def add_pathway(self, file):
        """
        param:file: kegg_pathway_list.txt
        """
        logging.info(f"parsing {file}")

        # parse kegg_pathway_list to df
        df = pd.read_csv(file, sep='\t', names=['ENTRY_ID', 'NAME'])
        df['ENTRY_ID']=df['ENTRY_ID'].str.replace('path:map', '')
        self.df = df

    def add_record(self, file):
        logging.info(f"parsing {file} ({len(self.df)})")

        if os.stat(file).st_size == 0:
            logging.warn(f"Skipped empty file: {file}")
            return

        def string_to_dict(s):
            d = {}
            for text in s:
                text = text.strip()
                key, vals = text.split(': ')
                d[key] = vals.split()
            return d

        # parse record to df
        df = pd.read_fwf(file, widths=[12, 500], comment="///")
        # get record type
        *k, ent_id, ent_type = df.columns[1].split()

        df.columns = ['HEADER', 'TEXT']
        df['HEADER'] = df['HEADER'].fillna(method='ffill')
        df['TEXT'] = df['TEXT'].astype(str).apply(
            lambda x: " ".join(x.split()))
        df = df.groupby(['HEADER'])['TEXT'].apply(
            lambda x: '          '.join(x)).reset_index()
        df = df.set_index(['HEADER'])
        df = df.T.reset_index()

        if 'PATHWAY' in df:
            df['PATHWAY'] = df['PATHWAY'].str.findall("\d{5}")
        if 'DBLINKS' in df:
            df['DBLINKS'] = df['DBLINKS'].str.split(
                '          ').apply(string_to_dict)
        if 'GENES' in df:
            df['GENES'] = df['GENES'].str.replace('[\(\)]', ' ', regex=True)
            df['GENES'] = df['GENES'].str.split(
                '          ').apply(string_to_dict)
        if 'ENZYME' in df:
            df['ENZYME'] = df['ENZYME'].str.split()
        if 'REACTION' in df:
            df['REACTION'] = df['REACTION'].str.split()

        # find all string columns and convert the string with new lines to a list
        for col in df.columns:
            if isinstance(df[col][0], str):
                df[col] = df[col].str.split('          ')

        # add ENTRY id and type
        df['ENTRY_ID'] = ent_id
        df['ENTRY_TYPE'] = ent_type

        df['ID'] = ent_id
        df = df.set_index(['ID'])

        self.df = self.df.append(df, sort=False)

    def to_json(self, outfile, **kwargs):
        self.df.to_json(outfile, orient='records', **kwargs)

    def to_dict(self, **kwargs):
        return self.df.to_dict('records', **kwargs)

    def prepare_values(self, df, col, db_prefix):
        """prepare_values
        param:df: pandas.df
        param:col: column name

        return: np.array of list of tuples
        """
        def _row_values(row, col):
            d = row[col]
            id = row['ENTRY_ID']
            return [(id, x, y) for x in d for y in d[x]]

        d = {}
        for value_sets in df.apply(_row_values, args=[col], axis=1).values:
            for value_set in value_sets:
                (id, v1, v2) = value_set
                if not f'{db_prefix}_{v1}' in d:
                    d[f'{db_prefix}_{v1}'] = []
                d[f'{db_prefix}_{v1}'].append(value_set)

        return d

    def prepare_values_for_info(self, cols):
        """prepare_values_for_info
        param:df: pandas.df
        param:col: columns name
        return: np.array of list of tuples
        """
        def _row_values(row, cols):
            data = []
            for col in cols:
                if col=='NAME':
                    value = row[col][0] if type(row[col]) is list else row[col]
                    data.append(value)
                else:
                    value = ", ".join(row[col]) if type(row[col]) is list else row[col]
                    data.append(value)

            return data

        return self.df.apply(_row_values, args=[cols], axis=1).values

    def prepare_genes_tables(self, df, col, db_prefix):
        """prepare_tables
        param:df: pandas.df
        param:col: column name
        return: true
        """
        uniq_orgs = []

        orglists = df.apply(lambda x: list(set(x[col])), axis=1).to_list()
        for orglist in orglists:
            uniq_orgs = list(set(uniq_orgs + orglist))

        for org in uniq_orgs:
            self.init_org_genes_table(f"{db_prefix}_{org}")

        return True

    def prepare_dblinks_tables(self, df, col, db_prefix):
        """prepare_tables
        param:df: pandas.df
        param:col: column name
        return: true
        """
        uniq_dbs = []

        db_lists = df.apply(lambda x: list(set(x[col])), axis=1).to_list()
        for db_list in db_lists:
            uniq_dbs = list(set(uniq_dbs + db_list))

        for db in uniq_dbs:
            self.init_dblink_table(f"{db_prefix}_{db}")

        return True

    def insert_pwy(self):
        data = self.prepare_values_for_info(['ENTRY_ID', 'NAME'])
        sql = f'INSERT OR IGNORE INTO kegg_pathway VALUES (?, ?)'
        self.c.executemany(sql, data)

    def insert_cpd_info(self):
        data = self.prepare_values_for_info(['ENTRY_ID', 'NAME', 'FORMULA', 'PATHWAY'])
        sql = f'INSERT OR IGNORE INTO c_info VALUES (?, ?, ?, ?)'
        self.c.executemany(sql, data)

    def insert_ec_info(self):
        data = self.prepare_values_for_info(['ENTRY_ID', 'SYSNAME', 'CLASS', 'COMMENT', 'PATHWAY'])
        sql = f'INSERT OR IGNORE INTO ec_info VALUES (?, ?, ?, ?, ?)'
        self.c.executemany(sql, data)

    def insert_ko_info(self):
        data = self.prepare_values_for_info(['ENTRY_ID', 'SYMBOL', 'NAME', 'PATHWAY'])
        sql = f'INSERT OR IGNORE INTO ko_info VALUES (?, ?, ?, ?)'
        self.c.executemany(sql, data)

    def insert_ko_genes(self):
        idx = self.df['GENES'].notnull()
        # prepare table
        self.prepare_genes_tables(self.df[idx], 'GENES', 'ko_genes')
        # prepare values
        table_value_dict = self.prepare_values(
            self.df[idx], 'GENES', 'ko_genes')

        for tablename in table_value_dict:
            sql = f'INSERT OR IGNORE INTO "{tablename}" VALUES (?, ?, ?)'
            self.c.executemany(sql, table_value_dict[tablename])

    def insert_ec_genes(self):
        idx = self.df['GENES'].notnull()
        # prepare table
        self.prepare_genes_tables(self.df[idx], 'GENES', 'ec_genes')
        # prepare values
        table_value_dict = self.prepare_values(
            self.df[idx], 'GENES', 'ec_genes')

        for tablename in table_value_dict:
            sql = f'INSERT OR IGNORE INTO "{tablename}" VALUES (?, ?, ?)'
            self.c.executemany(sql, table_value_dict[tablename])

    def insert_cpd_dblinks(self):
        idx = self.df['DBLINKS'].notnull()
        # prepare table
        self.prepare_dblinks_tables(self.df[idx], 'DBLINKS', 'cpd_dblinks')
        # prepare values
        table_value_dict = self.prepare_values(
            self.df[idx], 'DBLINKS', 'cpd_dblinks')
        
        for tablename in table_value_dict:
            sql = f'INSERT OR IGNORE INTO "{tablename}" VALUES (?, ?, ?)'
            self.c.executemany(sql, table_value_dict[tablename])

    def __del__(self):
        self.close_db()
