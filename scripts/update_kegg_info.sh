#/usr/bin/env bash
EXECDIR="$(cd "$(dirname "$0")" && pwd)"
PATH=$EXECDIR:$PATH

init_dir()
{
    local DIRNAME=$1
    [ ! -d $DIRNAME ] && mkdir -p $DIRNAME;
    [ ! -d $DIRNAME ] && { echo -e "ERROR: Directory $DIRNAME DOES NOT exists."; exit 1; }
}

usage()
{
    echo -e "usage: $0 [-h] [OPTIONS]

This is a bash wrapper to generate "kegg_info/" directory.
Available options are:

    '-k', '--kegginfo' <blast_db>
        The path to kegg_info location [required]

    '-w', '--overwrite'
        Download and overwrite existing files

    '-h', '--help'
        Help page
"
}

#### MAIN
WGETOPT="--waitretry=1 -t 3"
KEGGINFODIR="./kegg_info"
SKIP="-nc"

while [ "$1" != "" ]; do
    case $1 in
        -k | --kegginfo )           shift
                                    KEGGINFODIR=$1
                                    ;;
        -w | --overwrite )          shift
                                    SKIP=""
                                    ;;
        -h | --help )               usage
                                    exit
                                    ;;
        * )                         usage
                                    exit 1
    esac
    shift
done

if[ "$SKIP" != "" ]; then
    WGETOPT="$WGETOPT $SKIP"
fi

init_dir $KEGGINFODIR
cd $KEGGINFODIR

init_dir 'kegg_pathway_image'
init_dir 'kegg_pathway_kgml'
init_dir 'kegg_compound_image'
init_dir 'kegg_info_ec'
init_dir 'kegg_info_ko'
init_dir 'kegg_info_cpd'

# get pathway/ko/ec list
echo "-- Downloading pathway list..."
curl http://rest.kegg.jp/list/pathway > kegg_pathway_list.txt
echo "-- Downloading KO list..."
curl http://rest.kegg.jp/list/ko > kegg_ko_list.txt
echo "-- Downloading EC list..."
curl http://rest.kegg.jp/list/ec > kegg_ec_list.txt
echo "-- Downloading CPD list..."
curl http://rest.kegg.jp/list/compound > kegg_cpd_list.txt

# get images
echo "-- Downloading CPD images..."
parallel -j2 "wget --nc $WGETOPT -O kegg_compound_image/{}.png   http://rest.kegg.jp/get/{}/image"    ::: `cut -f1 kegg_cpd_list.txt | sed "s/cpd://"`
echo "-- Downloading pathway images..."
parallel -j2 "wget --nc $WGETOPT -O kegg_pathway_image/map{}.png http://rest.kegg.jp/get/map{}/image" ::: `cut -f1 kegg_pathway_list.txt | sed "s/path:map//"`

# get kgml
echo "-- Downloading EC-pathway XML..."
parallel -j2 "wget $WGETOPT -O kegg_pathway_kgml/ec{}.xml http://rest.kegg.jp/get/ec{}/kgml" ::: `cut -f1 kegg_pathway_list.txt | sed "s/path:map//"`
echo "-- Downloading KO-pathway XML..."
parallel -j2 "wget $WGETOPT -O kegg_pathway_kgml/ko{}.xml http://rest.kegg.jp/get/ko{}/kgml" ::: `cut -f1 kegg_pathway_list.txt | sed "s/path:map//"`

# get entry info
echo "-- Downloading KO entry..."
parallel -j2 "wget $WGETOPT -O kegg_info_ko/{}.txt  http://rest.kegg.jp/get/ko:{}"  ::: `cut -f1 kegg_ko_list.txt | sed "s/ko://"`
echo "-- Downloading EC entry..."
parallel -j2 "wget $WGETOPT -O kegg_info_ec/{}.txt  http://rest.kegg.jp/get/ec:{}"  ::: `cut -f1 kegg_ec_list.txt | sed "s/ec://"`
echo "-- Downloading CPD entry..."
parallel -j2 "wget $WGETOPT -O kegg_info_cpd/{}.txt http://rest.kegg.jp/get/cpd:{}" ::: `cut -f1 kegg_cpd_list.txt | sed "s/cpd://"`

# parsing EC/KO/CPD records
echo "-- Parsing EC/KO/CPD records and generate database..."
kegg_parser.py -k .

# generate LITE sqlite database
echo "-- Generating a LITE-version database..."
cp kegg_info.db kegg_info.lite.db
sqlite3 kegg_info.lite.db < $EXECDIR/db_lite_drop_tables.sql > db_lite_command.sql
echo "VACUUM;" >> db_lite_command.sql
sqlite3 kegg_info.lite.db < db_lite_command.sql

# cleaning up
echo "-- Cleaning up..."
rm -rf *_list.txt
rm -rf *_info_*
rm *.sql

echo "-- DONE"
