#!/usr/bin/env python3
import argparse as ap
import pandas as pd
import sqlite3
import numpy as np
import sys
import os, errno
import re
import json
import logging
import warnings
from collections import OrderedDict

__version__ = "0.6.1"

def parse_params(__version__):
    p = ap.ArgumentParser(
        prog=sys.argv[0], description=f"Omics Pathway Viewer (OPaver) v{__version__}")

    eg = p.add_mutually_exclusive_group(required=True)

    eg.add_argument(
        '-e', '--expression', metavar='FILE', nargs='*', type=str, default=None,
        help="""Expression data in plain text or excel format (.xls and .xlsx).""")

    eg.add_argument(
        '-s', '--show', metavar='OUTDIR', nargs='?', type=str,
        help="""Open OPaver GUI for a OPaver pre-computed dataset.""")

    p.add_argument(
        '-org', '--org', metavar='STR', type=str, default='KO',
        help="""Specify a general db EC/KO or a KEGG organism for the specific input ID of the gene [default: EC]""")

    p.add_argument(
        '-cdb', '--cdb', metavar='STR', type=str, default='CPD',
        help="""Specify a chemical DB for the specific input ID of the compound [default: CPD]""")

    p.add_argument(
        '-t', '--timepoint', metavar='STR', nargs='*', type=str, default=[],
        help="""Description of the timepoint [default: basenames of input files]""")

    p.add_argument(
        '-k', '--kegg', metavar='DIR', nargs='?', default="kegg_info",
        help="Pre-downloaded directory of KEGG files [default: kegg_info]")

    p.add_argument(
        '-db', '--db', metavar='FILE', nargs='?', default="",
        help="KEGG SQLite3 db [default: kegg_info/kegg_info.db]")

    p.add_argument(
        '-o', '--outdir', metavar='PATH', type=str, default='./',
        help="Output directory [default: ./opaver_output]")

    p.add_argument(
        '-p', '--port', metavar='PORT', type=int, default='5005',
        help="The port for Python http.server [default: 5005")

    p.add_argument(
        '-n', '--noserver', action='store_true', default=False,
        help="""Not automatically start a web server [default: None]""")

    p.add_argument(
        '--debug', action='store_true', default=False,
        help="""Debug mode [default: None]""")

    args_parsed = p.parse_args()

    if args_parsed.kegg and not args_parsed.db:
        args_parsed.db = f"{args_parsed.kegg}/kegg_info.db"

    return args_parsed


class Expression:
    """ The class of Expression info """

    # Collection of instances
    _member_list = []

    @classmethod
    def get_instances(cls):
        return cls._member_list

    @classmethod
    def empty_instances(cls):
        cls._member_list = []

    def __init__(self, exp_id, exp_orig_id, exp_type, exp_val, exp_name=""):
        self.exp_id = exp_id
        self.exp_orig_id = exp_orig_id
        self.exp_type = exp_type
        self.exp_val = exp_val
        self.exp_name = exp_name
        self.__class__._member_list.append(self)


class KeggEntry:
    """ The class of a KEGG Entry (enzyme or compound) """

    # Collection of instances
    _member_dict = {}

    @classmethod
    def get_by_key(cls, key):
        if key in cls._member_dict:
            return cls._member_dict[key]
        else:
            return None

    @classmethod
    def get_instances(cls):
        return cls._member_dict.values()

    @classmethod
    def empty_instances(cls):
        cls._member_dict = {}

    def __init__(self, eid, c):
        self.id = eid
        self.c = c
        self.exp = {}
        self.name = None
        self.e_class = None
        self.formula = None
        self.pathway = None
        self.definition = ""
        self.dict = {}
        self.org_type = None

        if re.search('^C\d{5}', self.id):
            self.ent_type = "compound"
        else:
            self.ent_type = "enzyme"

        try:
            # entry of compound
            if re.search('^C\d{5}', self.id):
                sql = "SELECT * FROM c_info WHERE ID=?"
                self.c.execute(sql, (self.id,))
                (eid, self.name, self.formula, self.pathway) = self.c.fetchone()
                self.org_type = "compound"
            # entry of KO
            elif re.search('^K\d{5}', self.id):
                sql = "SELECT * FROM ko_info WHERE ID=?"
                self.c.execute(sql, (self.id,))
                (eid, self.name, self.definition, self.pathway) = self.c.fetchone()
                self.org_type = "ko"
            # entry of EC
            elif re.search('^\d+\.\d+\.', self.id):
                sql = "SELECT * FROM ec_info WHERE ID=?"
                self.c.execute(sql, (self.id,))
                (eid, self.name, self.e_class, self.definition,
                    self.pathway) = self.c.fetchone()
                self.org_type = "ec"
            else:
                logger.warning(f"Skipped entry - unrecognized id: {self.id}.")
                del self
                return None

            if self.pathway==None or self.pathway == "":
                logger.warning(f"No pathway maps found for the entry {self.id}.")
                del self
            else:
                self.__class__._member_dict[eid] = self

        except sqlite3.Error as e:
            logger.error(f"Database error: {e}")
        except Exception as e:
            logger.error(f"Failed to init KeggEntry object. EID: {eid}, Exception in query: {e}.")
            del self


    def add_exp(self, exp):
        if not exp.exp_type in self.exp:
            self.exp[exp.exp_type] = []

        self.exp[exp.exp_type].append(exp)
        logger.debug(
            f"Expression object (id: {self.id}, type: {exp.exp_type}) added.")

    def get_types(self):
        return self.exp.keys()

    def get_exp_by_type(self, type):
        return self.exp[type]

    def get_pathway(self):
        return self.pathway

    def get_dict(self):
        # build info dict
        self.dict['info'] = {}
        self.dict['info']['type'] = self.ent_type
        self.dict['info']['name'] = self.name
        self.dict['info']['definition'] = self.definition
        self.dict['info']['pathway'] = self.pathway

        if self.ent_type == "enzyme":
            self.dict['info']['formula'] = self.formula

        # build dicts for different types
        for exp_type in self.get_types():
            if not exp_type in self.dict:
                self.dict[exp_type] = {}

            for exp in self.get_exp_by_type(exp_type):
                self.dict[exp_type][exp.exp_name] = {}
                self.dict[exp_type][exp.exp_name]["diff"] = exp.exp_val
                self.dict[exp_type][exp.exp_name]["orig_id"] = exp.exp_orig_id

        return self.dict


class PathwayMap:
    """ The class of a KEGG reference map """

    def __init__(self, mapid, map_type, c):
        self.mapid = mapid  # path:map#
        self.c = c
        self.name = ""
        self.entry = {}
        self.org_type = map_type

    def add_entry(self, entry):
        self.entry[entry.id] = entry
        logger.debug(entry.id)

        if entry.ent_type == "enzyme":
            if self.org_type != entry.org_type:
                logger.error( f"Inconsistent ID type found -- map{self.mapid} is '{self.org_type}' but '{entry.id}' entered.")
                sys.exit()

        if self.name == "":
            sql = "SELECT * FROM kegg_pathway WHERE ID=?"
            self.c.execute(sql, (self.mapid,))
            try:
                (mapid, self.name) = self.c.fetchone()
                return True
            except:
                logger.warning(f"SKIPPED: {entry.id} not found!")
                return False

    def get_entry(self):
        return self.entry.values()


class KeggMaps:
    """ The collection of kegg maps """

    def __init__(self, map_type, c):
        self.c = c
        self.maps = {}
        self.org_type = map_type

    def add_entry(self, entry):
        for mapid in entry.get_pathway().split(', '):
            if mapid == "":
                continue
            if not mapid in self.maps:
                self.maps[mapid] = PathwayMap(mapid, self.org_type, self.c)

            self.maps[mapid].add_entry(entry)

    def get_maps(self):
        return self.maps.values()

    def get_map_count(self):
        return len(self.maps)


class GeneCollection:
    """ The class of genes """

    def __init__(self):
        self.genes = {}
        self.maps = {}
        self.timepoint = []
        self.ec_cate = {}

    def __is_increasing(self, items):
        return all(i < j for i, j in zip(items, items[1:]))

    def __is_decreasing(self, items):
        return all(i > j for i, j in zip(items, items[1:]))

    def __all_positive(self, items):
        return all(item >= 0 for item in items)

    def __all_negative(self, items):
        return all(item < 0 for item in items)

    def add_exp(self, exp, timepoint, maps, c):
        if not timepoint in self.timepoint:
            self.timepoint.append(timepoint)
        if not exp.exp_name in self.maps:
            self.maps[exp.exp_name] = []
        if not exp.exp_name in self.genes:
            self.genes[exp.exp_name] = {}
        if not exp.exp_type in self.genes[exp.exp_name]:
            self.genes[exp.exp_name][exp.exp_type] = []
        if not exp.exp_name in self.ec_cate:
            self.ec_cate[exp.exp_name] = {}

        self.genes[exp.exp_name][exp.exp_type].append(exp.exp_val)
        comb_maps = self.maps[exp.exp_name]+maps
        self.maps[exp.exp_name] = list(set(comb_maps))

        if re.search('^K\d{5}', exp.exp_id):
            ent = KeggEntry.get_by_key(exp.exp_id)
            try:
                ec_parts = ent.definition.split('EC:')[1].split('.')
                self.ec_cate[exp.exp_name] = f"EC{ec_parts[0]}.{ec_parts[1]}"
            except:
                logger.debug(f"No EC number found for {exp.exp_id}")
        elif re.search('^C\d{5}', exp.exp_id):
            self.ec_cate[exp.exp_name] = "Compound.Cpd"
        else:
            ec_parts = exp.exp_id.split('.')
            self.ec_cate[exp.exp_name] = f"EC{ec_parts[0]}.{ec_parts[1]}"

    def get_map_gene_stat(self):
        maps_dict = {}
        for gid in self.genes:
            for etype in self.genes[gid]:
                for mapid in self.maps[gid]:
                    if mapid == "":
                        continue
                    if not mapid in maps_dict:
                        maps_dict[mapid] = {
                            "con_over": [],
                            "con_under": [],
                            "enh_over": [],
                            "enh_under": [],
                            "num_enhancing": 0,
                            "num_consistent": 0,
                            "num_all": 0,
                            "others": [],
                            "ec_stats": {}
                        }

                    exp_val_list = self.genes[gid][etype]
                    tol_tc_count = len(self.timepoint)

                    if len(exp_val_list) == tol_tc_count and self.__all_negative(exp_val_list):
                        if self.__is_decreasing(exp_val_list):
                            if not gid in maps_dict[mapid]["enh_under"]:
                                maps_dict[mapid]["enh_under"].append(gid)
                                maps_dict[mapid]["num_enhancing"] += 1
                        else:
                            if not gid in maps_dict[mapid]["con_under"]:
                                maps_dict[mapid]["con_under"].append(gid)
                                maps_dict[mapid]["num_consistent"] += 1
                    elif len(exp_val_list) == tol_tc_count and self.__all_positive(exp_val_list):
                        if self.__is_increasing(exp_val_list):
                            if not gid in maps_dict[mapid]["enh_over"]:
                                maps_dict[mapid]["enh_over"].append(gid)
                                maps_dict[mapid]["num_enhancing"] += 1
                        else:
                            if not gid in maps_dict[mapid]["con_over"]:
                                maps_dict[mapid]["con_over"].append(gid)
                                maps_dict[mapid]["num_consistent"] += 1
                    else:
                        if not gid in maps_dict[mapid]["others"]:
                            maps_dict[mapid]["others"].append(gid)

                    maps_dict[mapid]["num_all"] += 1

                    # counting ec categories
                    ec_cate_idx = self.ec_cate[gid]

                    if len(ec_cate_idx) > 0:
                        (c1, c2) = ec_cate_idx.split('.')
                        c2 = f"{c1}.{c2}"

                        if not c1 in maps_dict[mapid]["ec_stats"]:
                            maps_dict[mapid]["ec_stats"][c1] = {}
                        if not c2 in maps_dict[mapid]["ec_stats"][c1]:
                            maps_dict[mapid]["ec_stats"][c1][c2] = 0

                        maps_dict[mapid]["ec_stats"][c1][c2] += 1

        ordered_mapid = sorted(maps_dict, key=lambda m: (
            maps_dict[m]["num_enhancing"], maps_dict[m]["num_consistent"], maps_dict[m]["num_all"]), reverse=True)
        ordered_maps_dict = OrderedDict()
        for mapid in ordered_mapid:
            ordered_maps_dict[mapid] = maps_dict[mapid]

        return ordered_maps_dict

def symlink_force(target, link_name):
    try:
        os.symlink(target, link_name)
    except OSError as e:
        if e.errno == errno.EEXIST:
            os.remove(link_name)
            os.symlink(target, link_name)
        else:
            raise e

def start_http_server(proj, port):
    """ Start a http_server """
    import http.server
    import socketserver
    import webbrowser
    import threading
    import time

    proj_abspath = os.path.abspath(proj)
    proj_path = proj_abspath.rstrip('/')
    proj_name = os.path.basename(proj_path)

    bin_abspath = os.path.realpath(__file__)
    web_dir = os.path.join(os.path.dirname(bin_abspath), 'public')
    web_data_dir = os.path.join(web_dir, 'data')

    # prepare symlink for the project
    proj_symlink = f"{web_data_dir}/{proj_name}"
    if os.path.exists(proj_symlink):
        os.remove(proj_symlink)
    symlink_force(proj_path, proj_symlink)

    # starting http server
    Handler = http.server.SimpleHTTPRequestHandler
    PORT = port

    os.chdir(web_dir)
    url = f"http://127.0.0.1:{PORT}/?data={proj_name}"

    def start_server():
        with socketserver.TCPServer(("", PORT), Handler) as httpd:
            httpd.serve_forever()

    logger.info(f"Starting http_server at port {PORT}...")
    threaded = threading.Thread(target=start_server)
    threaded.start()

    time.sleep(1)

    if threaded.is_alive():
        logger.info(f"Opening web browser: {url}...")
        logger.info(f"Press [Ctrl-C] to exit the server.")
        webbrowser.open_new(url)

        while True:
            try:
                time.sleep(1)
            except KeyboardInterrupt:
                sys.exit(0)


def auto_id_conversion(id, org, cdb, c):
    """
    convert non KO/EC/CPD number to KO number
    """

    if (not id) or id=="0" or id=="" or str(id)=="nan":
        return np.nan
    elif re.search('^K\d{5}', id) or \
            re.search('^C\d{5}', id) or \
            re.search('^\d+\.\d+\.', id):
        return id
    else:
        # other DBs' compound id
        params = []
        if id.startswith(cdb):
            # an ID belongs to other chemical dbs is specified
            params = [x.strip() for x in id.split(':')]
            sql = f"SELECT DISTINCT ID FROM 'cpd_dblinks_{cdb}' WHERE DB like ? and CID like ?"
            logger.debug(f'searching cpd_dblinks_{cdb}')
        elif org.lower() == "ec" and ':' in id:
            # an ORG is specified in ID [ORG:xxx]
            params = [x.strip() for x in id.split(':')]
            sql = f"SELECT DISTINCT ID FROM 'ec_genes_{params[0]}' WHERE ORG like ? and GENE like ?"
            logger.debug(f'searching ec_genes_{params[0]}')
        elif org.lower() == "ko" and ':' in id:
            # an ORG is specified in ID [ORG:xxx]
            params = [x.strip() for x in id.split(':')]
            sql = f"SELECT DISTINCT ID FROM 'ko_genes_{params[0]}' WHERE ORG like ? and GENE like ?"
            logger.debug(f'searching ko_genes_{params[0]}')
        elif org.lower() != "ko" and org.lower() != "ec":
            # a specific ORG is given
            params = (org, id)
            sql = f"SELECT DISTINCT ID FROM 'ko_genes_{org}' WHERE ORG like ? and GENE like ?"
            logger.debug(f'searching ko_genes_{params[0]}')
        elif org.lower() == "ec":
            # search term from general EC info
            params = [f"%, {id}", id, f"{id}, %"]
            sql = f"SELECT DISTINCT ID FROM ec_info WHERE NAME like ? OR NAME like ? OR NAME like ?"
            logger.debug(f'searching ec_info')
        elif org.lower() == "ko":
            # search term from general KO info
            params = [f"%, {id}", id, f"{id}, %"]
            sql = f"SELECT DISTINCT ID FROM ko_info WHERE NAME like ? OR NAME like ? OR NAME like ?"
            logger.debug(f'searching ko_info')

        try:
            c.execute(sql, params)
        except sqlite3.Error as e:
            logger.error("Database error: %s" % e)
        except Exception as e:
            logger.error("Exception in _query: %s" % e)
            
        rows = c.fetchall()
        converted_id_list = [row[0] for row in rows]
        converted_id_list = list(set(converted_id_list))


        if len(converted_id_list) > 0:
            converted = " ".join(converted_id_list)
            logger.warning(f"Input entry id [{id}] converted to [{converted}]")
            return converted
        else:
            logger.warning(f"No associated ID found for {id}.")
            return np.nan


if __name__ == '__main__':
    warnings.simplefilter(action='ignore', category=FutureWarning)
    argvs = parse_params(__version__)
    org = argvs.org.lower()
    map_org = 'ec' if org=='ec' else 'ko'
    cdb = argvs.cdb

    log_level = logging.INFO
    if argvs.debug:
        log_level = logging.DEBUG

    # get logger
    logger = logging.getLogger(__name__)
    logger.setLevel(log_level)
    handler = logging.StreamHandler(sys.stdout)
    handler.setLevel(log_level)
    formatter = logging.Formatter(
        '%(asctime)s [%(levelname)s] %(message)s', '%H:%M:%S')
    handler.setFormatter(formatter)
    logger.addHandler(handler)

    # If expression files are specified, starting to process input files.
    # Otherwise, starting http server and load specified output directory to OPaver.
    if argvs.expression == None and argvs.show:
        start_http_server(argvs.show, argvs.port)
    else:
        # KEGG info db
        conn = sqlite3.connect(argvs.db)
        c = conn.cursor()

        # create output directory if not exists
        if not os.path.exists(argvs.outdir):
            os.makedirs(argvs.outdir)

        # load expression data of timepoint
        exp_dfs = OrderedDict()

        if argvs.expression[0].endswith('xls') or argvs.expression[0].endswith('xlsx'):
            logger.info(f"Excel file input detected.")
            exp_dfs = pd.read_excel(argvs.expression[0], sheet_name=None)
        else:
            logger.info(f"Plain text input detected.")
            cnt = 0
            for exp_file in argvs.expression:
                logger.info(f"Processing input file {exp_file}...")
                timepoint_name = ""
                if len(argvs.expression) == len(argvs.timepoint):
                    timepoint_name = argvs.timepoint[cnt]
                else:
                    basename = os.path.basename(exp_file)
                    basename = os.path.splitext(basename)[0]
                    timepoint_name = basename
                # real types start from the third columns
                logger.info(f"timepoint name: {timepoint_name}...")
                exp_dfs[timepoint_name] = pd.read_csv(
                    exp_file, sep=None, engine='python')
                cnt += 1

        logger.info(f"Total {len(exp_dfs)} input timepoint found.")

        # settgins of timepoint for OPaver webpage
        settings = {}
        overall_pwy = {}
        entry_datatable = {"data": []}
        gene_collection = GeneCollection()
        cnt = 0

        # load expression data of timepoint
        for timepoint_name in exp_dfs:
            logger.info(f"Processing timepoint {cnt}: {timepoint_name}...")
            ec_types = []
            cpd_types = []

            # clean up classes: Expression, KeggEntry
            Expression.empty_instances()
            KeggEntry.empty_instances()

            # get the dataframe of current timepoint
            exp_df = exp_dfs[timepoint_name]

            # get all entry types from the column names
            exp_types = exp_df.columns.values.tolist()[2:]

            # Handling unknown ids at the second column.
            # The converted ids store back to the second column.
            # The original entry id column duplicates to ENTRY_ORIG_ID column.
            exp_df.iloc[:, 1] = exp_df.iloc[:, 1].astype(str)
            exp_df['ENTRY_ORIG_ID'] = exp_df.iloc[:, 1]
            exp_df.iloc[:, 1] = exp_df.iloc[:, 1].apply(
                lambda id: auto_id_conversion(id, org, cdb, c))

            # create directory for this timepoint
            basename = f"exp_timepoint_{cnt}"
            timepoint_outdir = f"{argvs.outdir}/{basename}"
            if not os.path.exists(timepoint_outdir):
                os.makedirs(timepoint_outdir)

            # KeggMaps for the current timepoint
            kegg_maps = KeggMaps(map_org, c)

            # real types start from the third columns
            def _create_exp_object(x, exp_type):
                """
                create Expression object for each input row
                """
                eid_str = x.iloc[1]

                if eid_str and not '-' in eid_str:
                    eids = eid_str.split(' ')
                    for eid in eids:
                        Expression(eid, x['ENTRY_ORIG_ID'], exp_type, float(
                            x[exp_type]), x.iloc[0])

            for exp_type in exp_types:
                idxs = (exp_df[exp_type].notnull() &
                        exp_df.iloc[:, 1].notnull())
                df = exp_df[idxs]
                df.apply(_create_exp_object, args=[exp_type], axis=1)

            logger.info(
                f'{len(Expression.get_instances())} gene expression data loaded.')

            kegg_entry = None
            for exp in Expression.get_instances():
                if not KeggEntry.get_by_key(exp.exp_id):
                    logger.debug(exp.exp_id)
                    kegg_entry = KeggEntry(exp.exp_id, c)
                else:
                    kegg_entry = KeggEntry.get_by_key(exp.exp_id)

                try:
                    kegg_entry.add_exp(exp)
                    maps = kegg_entry.get_pathway().split(', ')
                    gene_collection.add_exp(exp, timepoint_name, maps, c)
                except:
                    continue

            logger.info(f'{len(KeggEntry.get_instances())} entries added.')

            for kegg_entry in KeggEntry.get_instances():
                # Keep the max number of types. While we render a enzyme/compound box,
                # we will divide the box by this number.
                if kegg_entry.ent_type == "enzyme":
                    ec_types = list(
                        set(ec_types + list(kegg_entry.get_types())))
                elif kegg_entry.ent_type == "compound":
                    cpd_types = list(
                        set(cpd_types + list(kegg_entry.get_types())))

                kegg_maps.add_entry(kegg_entry)

                # gather input data for entry_datatable
                for etype in kegg_entry.get_types():
                    for exp in kegg_entry.get_exp_by_type(etype):
                        edict = OrderedDict({
                            "time": timepoint_name,
                            "gene": exp.exp_name,
                            "id": exp.exp_id,
                            "difference": exp.exp_val,
                            "type": exp.exp_type,
                            "name": kegg_entry.name,
                            "map": kegg_entry.pathway
                        })
                        entry_datatable["data"].append(edict)

            logger.info(f'{kegg_maps.get_map_count()} KEGG maps found.')

            # save expression data of a map to JSON file
            out_pathway_list = {}
            for pathway_map in kegg_maps.get_maps():
                logger.debug(
                    f"Saving expression data for map{pathway_map.mapid}")
                out_entry = {}
                out_path_json = f"{timepoint_outdir}/map{pathway_map.mapid}.exp.json"

                for entry in pathway_map.get_entry():
                    out_entry[entry.id] = entry.get_dict()

                # dict for active pathway
                mapid = pathway_map.mapid
                if mapid == "":
                    continue

                out_pathway_list[mapid] = {}
                out_pathway_list[mapid]["active"] = len(out_entry)
                out_pathway_list[mapid]["name"] = pathway_map.name

                # dict for overall pwy json
                if not mapid in overall_pwy:
                    overall_pwy[mapid] = {}
                    timepoint_names = exp_dfs.keys()
                    # init a dict with values 0 for the number of active genes for all timepoint
                    overall_pwy[mapid]['active'] = dict.fromkeys(
                        timepoint_names, 0)
                    overall_pwy[mapid]['name'] = pathway_map.name

                overall_pwy[mapid]['active'][timepoint_name] = len(out_entry)

                # save expression data of a map to JSON file
                with open(out_path_json, 'w') as fp:
                    json.dump(out_entry, fp, indent=4, sort_keys=True)
                    fp.close()

                logger.debug(f'JSON file for mapd{mapid} saved.')

            logger.debug(
                f'JSON file for {kegg_maps.get_map_count()} KEGG maps saved.')

            # settings of the current
            settings[str(cnt)] = {}
            settings[str(cnt)]['name'] = basename
            settings[str(cnt)]['desc'] = timepoint_name
            settings[str(cnt)]['dir'] = basename
            settings[str(cnt)]['ec_type'] = sorted(ec_types)
            settings[str(cnt)]['cpd_type'] = sorted(cpd_types)
            settings[str(cnt)]['org_type'] = kegg_maps.org_type
            cnt += 1

        # saving settings.json
        out_path_json = f"{argvs.outdir}/settings.json"
        logger.debug(f"Saving {out_path_json}...")
        with open(out_path_json, 'w') as fp:
            json.dump(settings, fp, indent=4, sort_keys=True)
            fp.close()

        logger.info(f'Settings JSON file created: {out_path_json}.')

        # saving overall_pathway.json
        out_overall_pwy_json = f"{argvs.outdir}/overall_pathway.json"
        overall_pwy_list = []
        for mapid, value in overall_pwy.items():
            tc_active = [value['active'][tc]
                         for tc in sorted(value['active'].keys())]
            overall_pwy_list.append({
                "mapid": mapid,
                "sum_act": sum(tc_active),
                "tc_act": '/'.join(map(str, tc_active)),
                "name": value['name']
            })
        with open(out_overall_pwy_json, 'w') as fp:
            json.dump(overall_pwy_list, fp, indent=4, sort_keys=True)
            fp.close()

        logger.info(
            f'Overall map stats JSON file created: {out_overall_pwy_json}.')

        # saving expression data to datatable ajax json
        out_exp_datatable_json = f"{argvs.outdir}/exp_datatable.json"
        with open(out_exp_datatable_json, 'w') as fp:
            json.dump(entry_datatable, fp, indent=4, sort_keys=False)
            fp.close()

        logger.info(f'Datatable JSON file created: {out_exp_datatable_json}.')

        # saving expression data to datatable ajax json
        out_exp_gene_analysis_json = f"{argvs.outdir}/exp_gene_analysis.json"
        with open(out_exp_gene_analysis_json, 'w') as fp:
            map_gene_stat = gene_collection.get_map_gene_stat()
            json.dump(map_gene_stat, fp, indent=4, sort_keys=False)
            fp.close()

        logger.info(
            f'Gene analysis JSON file created: {out_exp_gene_analysis_json}.')

        # close
        conn.close()
        logger.info(f"Database committed and closed.")

        if not argvs.noserver:
            start_http_server(argvs.outdir, argvs.port)

        logger.info(f"Completed!")
