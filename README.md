# OPaver: Omics Pathway Viewer

OPaver is a web-based tool to integrate multiple types (e.g. transcriptomics and proteomics) and time series data to KEGG maps. OPaver is written in Python3 and leveraging Python's http.server module to provide a web-based interface for users. The image below is a preview screenshot for OPaver. A live demo is available at [https://poeli.gitlab.io/opaver-demo](https://poeli.gitlab.io/opaver-demo).

<img src='public/images/opaver_screenshot.png'/>

-------------------------------------------------------------------
## DEPENDENCY

In order to run OPaver correctly, your system requires to have following dependencies installed correctly.

- Python >= 3.6
- Pandas >= 0.23
- click
- openpyxl
- xlrd == 1.2.0 (optional for supporting Excel as input)

-------------------------------------------------------------------
## INSTALLATION

Clone opaver from the repo:

```bash
git clone https://gitlab.com/poeli/opaver.git
cd opaver
```

Install requirements using pip:

```bash
pip install -r requirements.txt
```

Please donwload a pre-compiled tar.gz file `kegg_info.tar.gz` that includes KEGG pathway info.

```bash
curl -O https://edge-dl.lanl.gov/opaver/kegg_info.tar.gz
tar -xzf kegg_info.tar.gz
```

(OPTIONAL) If your input includes KEGG gene names/other chemistry database, an extensive KEGG db is required:

```bash
curl -o kegg_info/kegg_info.db.gz https://edge-dl.lanl.gov/opaver/kegg_info.db.gz
rm kegg_info/kegg_info.db && gzip -d kegg_info/kegg_info.db.gz
```

-------------------------------------------------------------------
## UPDATE KEGG DATABASE

Required KEGG database and files in `kegg_info/` can be updated using the script `scripts/update_kegg_info.sh`.

-------------------------------------------------------------------
## QUICK START

The `opaver.py` is the main script to process input data and display results in the viewer. The following example will run a dataset that includes gene differential expression data using 2 methods at 3 time points. The data are stored in a excel spreadsheet `FC_WC.xlsx`. OPaver will output results to `test_opaver`. After the process completes, OPaver will start a web-server at the `localhost:5005`  and try to open the result page in your default browser. This dataset is available at the directory `test/test_dataset/`.

```bash
./opaver.py -e test/test_dataset/FC_WC.xlsx -org EC -o test_opaver
```

When you have OPaver's output in `test_opaver` directory, you can launch a web-server to display result directly using `--show [DIR]` option. To specify a port, use `-p [PORT]`:

```bash
./opaver.py -s test_opaver -p 6000
```

-------------------------------------------------------------------
## INPUT FORMAT

The input expression data can be put in `plain text file(s)` or a `Excel spreadsheet`.

The input file must follow the structure below. 
If you have multiple timepoints, separate each timepoint in a CSV/TSV file ().
Technically you can input as many methods as you want, but no more than 4 methods are recommended for genes. No more than 2 methods are allowed for compounds due to the space.
You can specify the corresponding names of the time series using `--timepoint` option (e.g. `--timepoint time1 time2 time3`), otherwise the basename of the input files will be used.

| Column | Header    | Required | Description                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             |
|--------|-----------|----------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| 1      | ENTRY     | Yes      | Gene name, locus, compound name or any text makes sense to you.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          |
| 2      | ID        | Yes      | The ID to map a gene or a compound on the KEGG pathway map; The acceptable ID can be one or mixed of following IDs. Note that you can't use both KO and EC at the same time. <ul><li>`KO`: KEGG Orthology identifier, like "K00231". Separate multiple values in `space`.</li><li>`EC`: Enzyme Commission number, like "1.1.1.1". Separate multiple values in `space`.</li><li>`GENE NAME`: A Gene symbol/name, like "PROX"</li><li>`[ORG]:[GENE ID/NAME]`: A organism specific gene symbol/id/name. For example, "DME: Dmel_CG3481", "DME:Adh" or "DER:6540581" are all acceptable.</li></ul> For metabolic compounds: <ul><li>`CPD`: KEGG compound identifier, like "C00047".</li><li>`[DB]:[ID]`: The corresponding ID in the other chemical DBs, like "PubChem:3371" or "ChEBI:16510".</li></ul> Note that you need to specify a chemical DB name using `-cdb [DB]` if you are not using KEGG compound identifier. |
| 3      | [method]  | Yes      | The value of the differential expression (DE) for the `method`, usually in log fold change. The `method` can be any name you want to display in OPaver. If multiple methods are provided (in separated columns), an KEGG entry-block will be split into the number of methods.                                                                                                                                                                                                                                                                                                                          |
| 4      | [method2] | No       | This column is optional for the second method of the DE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                |
| ...    | ...       | No       | This column is optional for the third method of the DE.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 |

OPaver supports excel spreadsheet ending for both .xls and .xlsx formats. The structure above is also required for using Excel spreadsheet as input. Each `tab` represent a time course, and the name of the tab is used as the name of the time series. Examples are provided in the directory `test/test_dataset/`.

-------------------------------------------------------------------
## CHANGE SETTINGS

The display settings can be changed by passing parameters to OPaver's web-server using HTTP GET method as `[parameter]=[value]`. For example, you can change color schemes to `RdBu` by adding `?color=RdBu` to the end of the url. Also check next section to see how to make changes for default settings.

| Parameter | Value                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
|-----------|------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|
| data      | The OPaver output directory locates at `public/data/`                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        |
| tc        | A 0-based serial number for each timepoint (0,1,2...). The default is `0` for the first timepoint.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                           |
| mapid     | A KEGG pathway map ID.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       |
| color     | The color scheme uses to highlight DGE values. OPaver supports any diverging color schemes in d3-scale-chromatic (e.g. `RdYlGn`); add `-r` to the end of the scheme name to reverse the color scale (e.g. `RdYlGn-r`). Examples are: <br/><img src="public/images/RdYlGn.png" width="100" height="20" alt="RdYlGn"/> RdYlGn<br/><img src="public/images/RdYlGn-inverse.png" width="100" height="20" alt="GnYlRd"/> RdYlGn-r <br/><img src="public/images/RdYlBu.png" width="100" height="20" alt="RdYlBu"/> RdYlBu <br/><img src="public/images/RdYlBu-inverse.png" width="100" height="20" alt="BuYlRd"/> RdYlBu-r <br/> The default color scheme is `RdYlGn-r`. For more color schemes, please refer to [d3/d3-scale-chromatic](https://github.com/d3/d3-scale-chromatic). |
| scaleMin  | The DGE smaller than the certain value will be represented using the leftmost color on the scale-bar. The default is `-2`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |
| scaleMax  | The DGE greater than the certain value will be represented using the rightmost color on the scale-bar. The default is `2`.                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                   |

-------------------------------------------------------------------
## CHANGE DEFAULT SETTINGS

The default settings can be changed by modifying the file `public/data/settings.json`.

```json
{
    "mapdiv": "keggmap",
    "tc": 0,
    "mapid": "",
    "color": "RdYlGn-r",
    "scaleMin": -2,
    "scaleMax": 2
}
```

-------------------------------------------------------------------
## MORE EXAMPLES

The DGE data of 3 time points are separated in 3 tsv files. Run opaver.py and specified the names of 3 time points to `ts1, ts2 and ts3`, and specify the output directory to `test`:
```bash
./opaver.py -e test/test_dataset/FC_WC_*.tsv -org ec -t ts1 ts2 ts3 -o test
```
Input a Excel spreadsheet of DE data that include genes annotated with EC# and compounds (KEGG CPD#):
```bash
./opaver.py -e test/test_dataset/FC_WC_ec_cpd3rdType.xlsx -org ec -k kegg_info -db kegg_info/kegg_info.db
```
Input a Excel spreadsheet of DE data that include genes annotated with EC# and 2 types of compounds:
```bash
./opaver.py -e test/test_dataset/FC_WC_ec2types_cpd2types.xlsx -org ec
```
Input a Excel spreadsheet of DE data that include the name of genes. Search these gene names for associated KO# in the name field of KO records:
```bash
./opaver.py -e test/test_dataset/FC_WC_gene_name.xlsx -org ko
```
Indicate input gene names are associated with organism `HSA`, but not showing results in the web-GUI:
```bash
./opaver.py -e test/test_dataset/FC_WC_gene_name.xlsx -org HSA -n
```
Specify `HSA` organism in the gene entry as `HSA:xxxxx`:
```bash
./opaver.py -e test/test_dataset/FC_WC_gene_name_HSA.xlsx -org ko
```
Specify `PubChem` as the input chemical ID database using `-cdb PubChem` and `PubChem:xxxxx` in the entry:
```bash
./opaver.py -e test/test_dataset/FC_WC_gene_name_CPD_DBLINKS.xlsx -org HSA -cdb PubChem
```

-------------------------------------------------------------------
## CONTACT

Please contact Po-E Li (po-e@lanl.gov) for any questions.
