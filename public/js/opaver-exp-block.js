class enzymeExp {
    // The class for rendering expression data at enzyme and ortholog entry

	constructor(eid, mapid, link, w, h, x, y, types, exp, hm, org, expColor) {
        this.eid = eid
        this.mapid = mapid
        this.link = link
        this.exp = exp
        this.hm = hm
        this.org = org
		this.pathway = exp.info.pathway
		this.desc = exp.info.definition
        this.name = exp.info.name
        this.types = types
        this.expColor = expColor
        this.map_w = w
        this.map_h = h
        this.map_x = x
        this.map_y = y
        this.dom = $(`<a target='new'>`)
        this.dom.addClass("enzyme-area")
        this.dom.attr("alt", this.name);
        this.dom.attr("href", this.link);
        this.dom.attr("title", this.name);
        this.dom.css("left", x+"px");
        this.dom.css("top", y+"px");
        this.dom.css("width", w+"px");
        this.dom.css("height", h+"px");
        this.dom.tooltipster({ interactive: true });
	}

	build_block(mapdiv) {
        // Generate an individual block for each expression type.
        
        // 1) create content for the tooltipster
        // adding KEGG enzyme information to the tooltip content
        let tooltipcontent = $('<div>');

        let idtag = $(`<span class="tag-label">${this.org}:${this.eid}</span>`);
        idtag.on("click", function () {
            $(`#${mapdiv}`).find("a.enzyme-area.glow").removeClass("glow");
            $(`#${mapdiv}`).find(`a.enzyme-area[alt*='${this.eid}']`).addClass("glow");
        });

        tooltipcontent.append(idtag);
        let tabledom = $('<table class="gene-info ui-responsive">');
        let tbodydom = $('<tbody>');
        $('<tr>')
            .append($('<td>').html('Name'))
            .append($('<td>').html(this.name))
            .appendTo(tbodydom);

        let pathwayTd = $('<td>');
        let pwys = this.pathway.split(", ");
        $.each(pwys, function (idx, pwy) {
            $('<a data-ajax="false" style="margin-right:0.5em">')
                .html(pwy)
                .on("click", function () {
                    if (pwy != this.mapid) {
                        drawMap(mapdiv, pwy, showicon, proj_dir, tc, this.hm)
                    } 
                })
                .appendTo(pathwayTd);
        });

        $('<tr>')
            .append($('<td>').html('Pathway'))
            .append(pathwayTd)
            .appendTo(tbodydom);
        $('<tr>')
            .append($('<td>').html('Desc'))
            .append($('<td>').html(this.desc))
            .appendTo(tbodydom);
        
        //tabledom.table();
        tabledom.append(tbodydom);
        tooltipcontent.append(tabledom);

        // 2) creating a div and filling a color reflecting the expression level
        // Iterates all expression types, adding following objects:
        //  - a block filling with color
        //  - counter icon
        //  - table for all expression data

        $.each(types, function (index, type) {
            // add a block of expression type and set to absolute coordinate of the wrapper
            let ex = w_block * index;
            let ey = 1;
            let block = $('<div class="type-block">')
            block.css("left", ex + "px");
            block.css("top", ey + "px");
            block.css("width", w_block + 1 + "px");
            block.css("height", (h + 1) + "px");

            // add counter icon
            if (typeof (annojson[id][type]) != "undefined" && Object.keys(annojson[id][type]).length >= showicon) {
                let ix = ex + w_block - 6;
                // for the first type, put the counter icon the left top corner.
                // Otherwise, put the counter icon to the right top corner.
                if (index == 0) { ix = -6 }
                let iy = -8;
                counterIcon = $('<span class="badge badge-primary gene-number-icon">').html(0)
                let num = counterIcon.html();
                let tolnum = Number(num) + Object.keys(annojson[id][type]).length;
                counterIcon.html(tolnum);
                counterIcon.css("left", ix + "px");
                counterIcon.css("top", iy + "px");

                // determining if there is a conflict of up- and down-regulation
                let sign = 0
                $.each(annojson[id][type], function (gene, ginfo) {
                    if (!sign) {
                        sign = Math.sign(ginfo.diff)
                    }
                    else {
                        sign_current = Math.sign(ginfo.diff)
                        if (sign_current != sign) {
                            counterIcon.removeClass("badge-primary");
                            counterIcon.addClass("badge-warning");
                            return
                        }
                    }
                });

                counterIcon.appendTo(ec_wrapper_dom)
            }

            // add expression data
            if ($(annojson[id][type]).length) {
                tooltipcontent.append($('<span>').html(`${type} (${Object.keys(annojson[id][type]).length})`));

                let tabledom = $('<table data-role="table" data-mode="reflow" class="gene-info ui-responsive">');
                let theaddom = $('<thead>');
                $('<tr>').append($('<th>').html('Gene'))
                    //.append( $('<th>').html('P-value') )
                    .append($('<th>').html('Difference'))
                    .appendTo(theaddom);

                let display_exp_lvl;
                let max_exp_lvl = 0;
                let tbodydom = $('<tbody>');
                $.each(annojson[id][type], function (gene, ginfo) {
                    let diff = Number(ginfo.diff)
                    let url_gene = `<a href="https://www.ncbi.nlm.nih.gov/gene/?term=${gene}" target="_new">${gene}</a>`;
                    
                    // adding Gene records to the tooltip tables
                    $('<tr>').append($('<td>').html(url_gene))
                        //.append( $('<td>').html( ginfo.pvalue ) )
                        .append($('<td>').html(diff))
                        .appendTo(tbodydom);

                    // counting up- or down-regulation genes
                    if ( diff > 0 ){
                        up_reg_num ++;
                    }
                    else{
                        dn_reg_num ++;
                    }

                    // Filling block with the color reflecting the expression level.
                    // pos-max
                    // neg-min
                    // abs-max (default)
                    if ( Object.keys(annojson[id][type]).length > 1 ){
                        // get the max absolute level
                        if (Math.abs(diff) >= max_exp_lvl) {
                            max_exp_lvl = Math.abs(diff);
                        }

                        if (display_exp_lvl==undefined){
                            display_exp_lvl = diff;
                        }

                        if (conflict_handler == 'pos-max'){
                            if (diff > 0 && Math.abs(diff) >= display_exp_lvl) {
                                display_exp_lvl = diff
                            }
                        }
                        else if (conflict_handler == 'neg-min'){
                            if (diff < 0 && Math.abs(diff) <= display_exp_lvl) {
                                display_exp_lvl = diff
                            }
                        }
                    }
                    else {
                        display_exp_lvl = diff
                    }

                    if ( !display_exp_lvl ){
                        display_exp_lvl = max_exp_lvl
                    }

                    block.css("background-color", exp_color.get_rgb(display_exp_lvl));
                });

                //tabledom.table();
                tabledom.append(theaddom);
                tabledom.append(tbodydom);
                tooltipcontent.append(tabledom);
            }

            ec_wrapper_dom.append(block)
        })

        // A EC number tag
        let dom = `<div><div>${id}</div></div>`;
        let ec_tag_dom = $(dom);
        ec_tag_dom.addClass("enzyme-area-number-tag");
        ec_tag_dom.css("top", "1px");
        ec_tag_dom.css("width", w+1 + "px");
        ec_tag_dom.css("height", h+1 + "px");
        ec_wrapper_dom.append(ec_tag_dom)

        if (ids.length && is_global_overview_map(mapid)==false ) {
            ec_wrapper_dom.tooltipster('destroy');
            ec_wrapper_dom.tooltipster({ content: tooltipcontent, arrow: false, interactive: true, offsetY: -100, position: 'right' });
        }
    }
    
    render(mapdiv) {
        this.dom.appendTo($(`#${mapdiv}`));
    }
}

class compoundExp {
}